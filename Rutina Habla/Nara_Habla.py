# [START import_libraries]
# from __future__ import division

# import re
# import sys

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
# from six.moves import queue
import argparse
import wave
import sys
import pygame as pg
# [END import_libraries]

# Global Variables
global Texto
Texto = "Nara es una perra que le gusta salir a pasear"



def Asistente_Home(Texto, nombrearchivo):
    nombrearchivo = nombrearchivo + ".mp3"
    GeneraMp3(Texto)
    #Habla(nombrearchivo, 1)
    return


# [START tts_synthesize_text]
def GeneraMp3(text):
	print(text)

#    """Synthesizes speech from the input string of text."""
	from google.cloud import texttospeech
	client = texttospeech.TextToSpeechClient()
	input_text = texttospeech.types.SynthesisInput(text=text)

    # Note: the voice can also be specified by name.
    # Names of voices can be retrieved with client.list_voices().

	voice = texttospeech.types.VoiceSelectionParams(
        language_code='es-CL',
        ssml_gender=texttospeech.enums.SsmlVoiceGender.FEMALE)
	audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3)
	response = client.synthesize_speech(input_text, voice, audio_config)

    # The response's audio_content is binary.
	with open('output.mp3', 'wb') as out:
		out.write(response.audio_content)
		print('Audio content written to file output.mp3')

        
# [END tts_synthesize_text]

def Habla(music_file, volume):
    '''
    stream music with mixer.music module in a blocking manner
    this will stream the sound from disk while playing
    '''
    # set up the mixer
    freq = 22000     # audio CD quality 44100
    bitsize = -8    # unsigned 16 bit -16
    channels = 2     # 1 is mono, 2 is stereo
    buffer = 2048    # number of samples (experiment to get best sound) 2048
    pg.mixer.init(freq, bitsize, channels, buffer)
    # volume value 0.0 to 1.0
    pg.mixer.music.set_volume(volume)
    clock = pg.time.Clock()
    try:
        pg.mixer.music.load(music_file)
        print("Music file {} loaded!".format(music_file))
    except pg.error:
        print("File {} not found! ({})".format(music_file, pg.get_error()))
        return
    pg.mixer.music.play()
    while pg.mixer.music.get_busy():
        # check if playback has finished
        clock.tick(30)
# pick a MP3 music file you have in the working folder
# otherwise give the full file path
# (try other sound file formats too)

# optional volume 0 to 1.0
#	volume = 1.0
